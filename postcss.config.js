'use strict';

const autoprefixer = require('autoprefixer');
const cssnano      = require('cssnano');
const cssnext      = require('postcss-preset-env');
const webpInCss    = require('webp-in-css/plugin');

const plugins = {
  'development': [
    // webpInCss()
  ],
  'production':  [
    cssnext(),
    // webpInCss(),
    autoprefixer({grid: 'autoplace'}),
    cssnano()
  ]
};


module.exports = {
  plugins: plugins[process.env.NODE_ENV],
  map:     process.env.NODE_ENV === 'production' ? false : 'inline'
};
