// require('webp-in-css');
import './parts/polyfills';

import { initLazyLoad } from './parts/lazy-load';

document.addEventListener('DOMContentLoaded', () => {
  window.IS_MOBILE = window.innerWidth < 768;

  initLazyLoad();
});
