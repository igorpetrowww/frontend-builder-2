require('intersection-observer');

// Меняем srcset в sources и src в img
function initPicturesLazyLoad() {
  const handler = entries => {
    entries.forEach(entry => {
      if (entry.intersectionRatio > 0) {
        const $target = entry.target;

        const $sources = $target.querySelectorAll('source');
        $sources.forEach(source => {
          const srcReal = source.getAttribute('data-srcset');
          source.setAttribute('srcset', srcReal);
        });

        const $images = $target.querySelectorAll('img');
        $images.forEach(image => {
          const srcReal = image.getAttribute('data-src');
          image.setAttribute('src', srcReal);
        });

        pictureObserver.unobserve(entry.target);
      }
    });
  };

  const pictureObserver = new IntersectionObserver(handler, { rootMargin: '1000px' });

  document.querySelectorAll('.js-lazy-picture')
    .forEach(img => pictureObserver.observe(img));
}


export function initLazyLoad() {
  initPicturesLazyLoad();
}
