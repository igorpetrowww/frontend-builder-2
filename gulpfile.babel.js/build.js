import { src, dest, parallel } from 'gulp';
import { getDir, handleError, isProd } from './helpers';
import pug from 'gulp-pug';
import sass from 'gulp-sass';
import sassGlob from 'gulp-sass-glob';
import postcss from 'gulp-postcss';
import webpack from 'webpack-stream';
import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import imagemin from 'gulp-imagemin';
import webp from 'gulp-webp';
import typograf from 'gulp-typograf';
import beautify from 'gulp-html-beautify';
import cached from 'gulp-cached';
// import print from 'gulp-print';

import siteData from '../src/data';

export function buildPages(cb) {
  let pagesToBuild = 'src/pages/*.pug';

  if (process.env.NODE_ENV === 'production') {
    pagesToBuild = 'src/pages/[^_]*.pug';
  }

  src(pagesToBuild)
    .pipe(pug({
      locals: siteData,
      pretty: true,
    })
      .on('error', handleError))
    .pipe(beautify({
      indent_size: 2,
      indent_char: ' ',
      indent_level: 0,
      preserve_newlines: true,
      max_preserve_newlines: 2,
      end_with_newline: true,
    }))
    .pipe(typograf({ locale: ['ru'] }))
    .pipe(dest(`${getDir()}/`));

  cb();
}

export function buildManifest(cb) {
  src('src/pages/manifest.json')
    .pipe(dest(`${getDir()}/`));
  cb();
}

export function buildVendorScripts(cb) {
  src('./src/scripts/vendor/**/*.js')
    .pipe(cached('scriptsVendor'))
    .pipe(dest(`${getDir()}/scripts/vendor/`));
  cb();
}

export function buildScripts(cb) {
  let stream = src('./src/scripts/index.js');

  stream = stream
    .pipe(webpack({
      mode: process.env.NODE_ENV || 'development',
      output: { filename: 'bundle.js' },
    })
      .on('error', handleError));

  if (isProd()) {
    stream
      .pipe(babel({ presets: ['@babel/env'] }))
      .pipe(uglify({ mangle: false }));
  }

  stream.pipe(dest(`${getDir()}/scripts/`));

  cb();
}

export function buildStyles(cb) {
  src('./src/styles/*.scss')
    .pipe(sassGlob())
    .pipe(sass()
      .on('error', handleError))
    .pipe(postcss()
      .on('error', handleError))
    .pipe(dest(`${getDir()}/styles/`));

  cb();
}

export function buildFavicons(cb) {
  src('src/assets/favicon/**/*.*')
    .pipe(cached('favicons'))
    .pipe(dest(`${getDir()}/assets/favicon/`));
  cb();
}

export function buildVideo(cb) {
  src('src/assets/video/**/*.*')
    .pipe(cached('videos'))
    .pipe(dest(`${getDir()}/assets/video/`));
  cb();
}

export function buildOther(cb) {
  src('src/assets/other/**/*.*')
    .pipe(cached('otherAssets'))
    .pipe(dest(`${getDir()}/assets/other/`));
  cb();
}

export function buildFonts(cb) {
  src('src/assets/fonts/**/*.*')
    .pipe(cached('fonts'))
    .pipe(dest(`${getDir()}/assets/fonts/`));
  cb();
}

export function buildImages(cb) {
  src('src/assets/img/**/*.*')
    .pipe(cached('img'))
    .pipe(imagemin())
    .pipe(dest(`${getDir()}/assets/img/`));

  src('src/assets/img/**/*.{jpg,png}')
    .pipe(cached('imgWebp'))
    .pipe(webp({ quality: 75 }))
    .pipe(dest(`${getDir()}/assets/img/`));

  cb();
}

exports.buildAssets = parallel(
  buildImages,
  buildFavicons,
  buildFonts,
  buildVideo,
  buildOther,
);
