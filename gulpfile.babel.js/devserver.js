import browserSync from 'browser-sync';

const browserSyncInstance = browserSync.create();

export function runDevServer(cb) {
  const params = {
    watch: true,
    open: false,
    reloadDebounce: 150,
    notify: false,
    server: { baseDir: './dev' },
  };

  const callback = (err, bs) => {
    bs.addMiddleware('*', (req, res) => {
      res.writeHead(302, {
        location: '404.html',
      });
      res.end('Redirecting!');
    });
  };

  browserSyncInstance.init(params, callback);

  cb();
}
