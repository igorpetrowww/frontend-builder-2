import { series, parallel } from 'gulp';
import {
  clearBuild,
  createBlock,
  setDevelopment,
  setProduction,
} from './helpers';
import {
  buildAssets,
  buildManifest,
  buildPages,
  buildScripts,
  buildStyles, buildVendorScripts,
} from './build';
import { runDevServer } from './devserver';
import { watchSrc } from './watchers';
// import webp from 'gulp-webp';

exports.createBlock = createBlock;

exports.test = series(buildPages);

exports.build = series(
  clearBuild,
  setProduction,
  parallel(buildPages, buildManifest, buildVendorScripts, buildScripts, buildStyles, buildAssets),
);

exports.default = parallel(
  runDevServer,
  watchSrc,
  setDevelopment,
  parallel(buildPages, buildManifest, buildVendorScripts, buildScripts, buildStyles, buildAssets),
);
