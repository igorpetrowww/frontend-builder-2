import { watch } from 'gulp';
import {
  buildFavicons,
  buildFonts,
  buildImages,
  buildManifest,
  buildOther,
  buildPages,
  buildScripts,
  buildStyles,
  buildVendorScripts,
  buildVideo,
} from './build';

const watchers = [
  [['src/pages/**/*.pug', 'src/blocks/**/*.pug'], buildPages],
  [['src/pages/manifest.json'], buildManifest],
  [['src/styles/**/*.scss', 'src/blocks/**/*.scss'], buildStyles],
  [['src/scripts/**/*.js', '!src/scripts/vendor/**/*.js', 'src/blocks/**/*.js'], buildScripts],
  [['src/scripts/vendor/**/*.js'], buildVendorScripts],
  [['src/assets/img/**/*.*'], buildImages],
  [['src/assets/favicon/**/*.*'], buildFavicons],
  [['src/assets/video/**/*.*'], buildVideo],
  [['src/assets/fonts/**/*.*'], buildFonts],
  [['src/assets/other/**/*.*'], buildOther],
];

export function watchSrc(cb) {
  watchers.forEach(watcher => {
    watch(watcher[0], watcher[1]);
  });

  cb();
}
