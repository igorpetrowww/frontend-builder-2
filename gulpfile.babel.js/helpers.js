import fs from 'fs';
import del from 'del';
import notifier from 'node-notifier';
import minimist from 'minimist';

const inputArguments = minimist(process.argv.slice(2));
const params = {
  regExpShortPath: new RegExp('.*\/(.*\/.*)'),
};


export function setDevelopment(cb) {
  process.env.NODE_ENV = 'development';
  cb();
}

export function setProduction(cb) {
  process.env.NODE_ENV = 'production';
  cb();
}

export function isProd() {
  return process.env.NODE_ENV === 'production';
}

export function getDir() {
  return isProd() ? 'build' : 'dev';
}

export function createBlock(cb) {
  if (!inputArguments.names) {
    console.log('\x1b[31m', 'Передайте названия блоков в аргументе --names "block-1, block-2"');
    return false;
  }

  inputArguments.names.split(', ')
    .forEach(blockName => {
      const createFiles = () => {
        fs.appendFile(`./src/blocks/${blockName}/${blockName}.pug`, '', () => {});
        fs.appendFile(`./src/blocks/${blockName}/${blockName}.scss`, '', () => {});
        console.log('\x1b[32m', `${blockName} ✓`);
      };

      fs.mkdir(`./src/blocks/${blockName}/`, { recursive: true }, createFiles);
    });

  cb();
}

export function clearBuild() {
  return del('./build');
}

export function handleError(error) {
  const shortPath = error.file ? error.file.match(params.regExpShortPath)[1] : 'no file';
  const shortSource = error.source ? error.source.substring(error.source.length - 25) : '';

  console.warn('\x1b[31m', 'Error!');

  return notifier.notify({
    title: error.name,
    subtitle: `${shortPath}:${error.line}`,
    message: `${error.reason || error.message}: "...${shortSource}"`,
    timeout: 12,
  });
}
